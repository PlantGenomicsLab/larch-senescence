# Contaminant Filtering

## Summary table

| **Species** | **Individual name** | **Timepoint** | **Location** | **Biological replicate** |
| ------ | ------ | ------ | ------ | ------ | 
| *Larix laricina* | K11 | 1 | K | 1 |
| *Larix laricina* | K12 | 2 | K | 1 | 
| *Larix laricina* | K13 | 3 | K | 1 | 
| *Larix laricina* | K21 | 1 | K | 2 | 
| *Larix laricina* | K22 | 2 | K | 2 | 
| *Larix laricina* | K23 | 3 | K | 2 |
| *Larix laricina* | K31 | 1 | K | 3 |  
| *Larix laricina* | K32 | 2 | K | 3 | 
| *Larix laricina* | K32 | 3 | K | 3 | 
| *Larix laricina* | U11 | 1 | U | 1 |
| *Larix laricina* | U12 | 2 | U | 1 | 
| *Larix laricina* | U13 | 3 | U | 1 | 
| *Larix laricina* | U21 | 1 | U | 2 | 
| *Larix laricina* | U22 | 2 | U | 2 | 
| *Larix laricina* | U23 | 3 | U | 2 |
| *Larix laricina* | U31 | 1 | U | 3 |  
| *Larix laricina* | U32 | 2 | U | 3 | 
| *Larix laricina* | U32 | 3 | U | 3 |  

## Reference transcriptome for *Larix laricina*

| **Total assembled transcripts** | **Total transcripts after frame selection** | **Total transcripts after clustering** | **Total transcripts after 300 pb filtering** | **Total transcripts after contaminant filtering (EnTAP, fungi,bacteria,insecta,amoebozoa)** |
| ------ | ------ | ------ | ------ | ------ |
| 1135379 | 832974 | 278727 | 276015 | 209431 |

## Percentage of pseudoaligned reads against the reference transcriptome per sample (number proccessed 16846379)

| **Individual name** | **Timepoint** | **Location** | **Biological replicate** | **Percentage of pseudoaligned reads** | **Number of pseudoaligned reads** |
| ------ | ------ | ------ | ------ | ------ | ------ |
| K11 | 1 | K | 1 | 65.2 | 10803475 (number proccessed 16566626) |
| K12 | 2 | K | 1 | 62.3 | 9715924 (number proccessed 15595831) |
| K13 | 3 | K | 1 | 46.9 | 8531726 (number proccessed 18206091) |
| K21 | 1 | K | 2 | 66.3 | 9763089 (number proccessed 14720290) |
| K22 | 2 | K | 2 | 58.5 | 9239564 (number proccessed 15782439) |
| K23 | 3 | K | 2 | 69.2 | 11650317 (number proccessed 16846379) |
| K31 | 1 | K | 3 | 47.7 | 7967958 (number proccessed 16713353) |
| K32 | 2 | K | 3 | 31.3 | 4800624 (number proccessed 15328874) |
| K33 | 3 | K | 3 | 24.2 | 4583835 (number proccessed 18975330) |
| U11 | 1 | U | 1 | 74.0 | 13225011 (number proccessed 17875190) |
| U12 | 2 | U | 1 | 66.8 | 12377871 (number proccessed 18530688) |
| U13 | 3 | U | 1 | 74.6 | 13616634 (number proccessed 18258192) |
| U21 | 1 | U | 2 | 71.4 | 12408060 (number proccessed 17387904) |
| U22 | 2 | U | 2 | 70.6 | 12016671 (number proccessed 17016404) |
| U23 | 3 | U | 2 | 74.0 | 11696886 (number proccessed 15800582) |
| U31 | 1 | U | 3 | 69.1 | 12703401 (number proccessed 18376662) |
| U32 | 2 | U | 3 | 69.5 | 12504195 (number proccessed 17990564) |
| U33 | 3 | U | 3 | 67.9 | 12098417 (number proccessed 17822560) |

**Total reads aligning for each library to its reference** | **Total % annotated from Sim Search alone** | **Total % annotated from Sim Search + gene family** |
Following this tutorial: https://github.com/CBC-UCONN/RNAseq_nonmodel

Tip to decide how much memory and cores request in all the jobs of this pipeline: 

Seff + job ID (allows you to check how much memory and how many cores your job is using and thus help you decide how much/many request)

1. **Identifying the Coding Regions**
Our goal in this portion of the tutorial is to create a single reference transcriptome containing one reference transcript per gene to quantify gene expression in all six samples against. We need to identify the coding regions of the transcripts for the first step in this process because we'll perform the clustering of redundant transcripts within and among samples using coding sequences. Later on, we'll also use amino acid sequences to do annotation using EnTAP.

2. **Identifying coding regions using TransDecoder and hmmer**

Before looking for the coding regions we'll combine all the assemblies together. Because of Trinity's naming convention, there are likely to be identical sequence names appearing in each assembly. These redundant names would confuse TransDecoder and turn some of its output into nonsense. Worse, this error would occur silently and be propagated to the rest of our analyses. To deal with this, we'll simply add a sample ID prefix to each sequence name using the linux utility sed.

```
Code here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/SampleID.sh
```

Now we can concatenate the assemblies into a single file:

```
Code here:
/core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/Concatenate.sh
```

Now that we have our reads assembled and combined together into the single file, we can run TransDecoder and hmmer to identify likely coding regions. There are three steps here. First, identify long open reading frames (ORFs) using TransDecoder.LongOrfs. Many transcripts will have multiple ORFs that could correspond to a true coding sequence. Second, identify ORFs with homology to known proteins using hmmer. Third, use TransDecoder.Predict to generate a final set of candidate coding regions.

To find all long ORFs, we run TransDecoder.LongOrfs like this:

```
Code here:
/core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/TransdecoderLongORF.sh
```

By default it will identify ORFs that are at least 100 amino acids long (you can change this by using -m parameter). It will produce a folder called trinity_combine.fasta.transdecoder_dir.

```
04_Coding_Regions
├── trinity_combine.fasta.transdecoder_dir
│   ├── base_freqs.dat
│   ├── longest_orfs.cds
│   ├── longest_orfs.gff3
│   └── longest_orfs.pep

```

Many transcripts are likely to have multiple ORFs, so it will be helpful to bring in evidence of homology to known proteins for the final predictions. We'll do this by searching the Pfam database. Pfam stands for "Protein families", and is a massive database with mountains of searchable information on, well, you guessed it, protein families. This will maximize the sensitivity for capturing the ORFs that have functional significance. We can scan the Pfam database using the software hmmer. The Pfam database is much too large to install on a local computer. We have a copy on Xanadu, however, in the directory */isg/shared/databases/Pfam/Pfam-A.hmm.* We'll run hmmer using the following code:

```
Code:
/core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/hmmer.sh
```

Once the run is completed it will create the following files in the directory.

```
04_Coding_Regions
├── pfam.domtblout
```

Lastly we use the 'TransDecoder.Predict' function along with our hmmer output to make final predictions about which ORFs in our transcripts are real.

```
TransDecoder.Predict -t ../Assembly/trinity_combine.fasta \
        --retain_pfam_hits pfam.domtblout \
        --cpu 16

```

```
Code: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/TransdecoderPredict.sh
```

This will add output to our trinity_combine.fasta.transdecoder_dir (located at: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/trinity_combine.fasta.transdecoder_dir), which now looks like:

```
04_Coding_Regions/
├── pfam.domtblout
├── pipeliner.38689.cmds
├── pipeliner.5719.cmds
├── pipeliner.63894.cmds
├── trinity_combine.fasta.transdecoder.bed
├── trinity_combine.fasta.transdecoder.cds
├── trinity_combine.fasta.transdecoder_dir/
│   ├── base_freqs.dat
│   ├── hexamer.scores
│   ├── longest_orfs.cds
│   ├── longest_orfs.cds.best_candidates.gff3
│   ├── longest_orfs.cds.best_candidates.gff3.revised_starts.gff3
│   ├── longest_orfs.cds.scores
│   ├── longest_orfs.cds.top_500_longest
│   ├── longest_orfs.cds.top_longest_5000
│   ├── longest_orfs.cds.top_longest_5000.nr
│   ├── longest_orfs.gff3
│   └── longest_orfs.pep
├── trinity_combine.fasta.transdecoder.gff3
└── trinity_combine.fasta.transdecoder.pep
```

TransdecoderPredict has created a frame selected protein file containing the coding regions of the transcripts for all the libraries (trinity_combine.fasta.transdecoder.pep).

3. **Determining and Removing Redundant Transcripts** 

De novo transcriptome assemblies are complex, containing both biological variation in the form of alternately spliced transcripts and nucleotide sequence variation, and technical issues such as fragmented transcripts. In this tutorial we have six de novo transcriptome assemblies. We're aiming to quantify gene expression at the gene level, so ideally we want to winnow out much of this complexity and create a single transcriptome with one transcript representing each underlying gene against which we can quantify gene expression for all six samples. In the previous step, we identified candidate coding regions for transcripts from all six samples. In this step, we'll cluster all those transcripts by amino acid sequence and select a single one to represent each cluster.

To this end, we used *vsearch* to cluster the transcripts with similar sequences (similarity is set by the identity between the sequences --id) and then choose one representative transcript (the centroid). A more detailed account of the application can be found at: vsearch. The threshold for clustering in this example is set to 90% identity. We run vsearch using the following code:

Code here: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/ClusteringVsearch.sh*

```
!/bin/bash
# Submission script for Xanadu
###SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=vsearch
#SBATCH -o vsearch-%j.output
#SBATCH -e vsearch-%j.error
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G

module load vsearch/2.4.3

vsearch --threads 16 --log LOGFile \
        --cluster_fast /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/trinity_combine.fasta.transdecoder.cds \
        --id 0.90 \
        --centroids centroids.fasta \
        --uc clusters.uc

```
At the end of the run it will produce the following files:

```
05_Clustering/
├── centroids.fasta
├── clusters.uc
├── combine.fasta
└── LOGFile
```

The centroids.fasta file will contain the representative transcripts from the 6 assemblies.

4. **Evaluating the Assembly**
Now that we've settled on our reference transcriptome (centroids.fasta), we can assess its quality. We will benchmark it against a gene database using rnaQUAST. This will compare the transcripts with a gene database and will produce a summary report.

```
module load rnaQUAST/2.2.0

/isg/shared/apps/rnaQUAST/2.2.0/rnaquast-2.2.0/rnaQUAST.py --transcripts /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/centroids.fasta --gene_mark --threads 16 --output_dir rnaQUAST

```

Code here: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/rnaQUAST.sh*

This is the output directory:

```
rnaQuast/
├── results/
│   ├── centroids_output/
│   │   ├── alignment_metrics.txt
│   │   └── txt_dir/
│   ├── logs/
│   │   └── rnaQUAST.log
│   ├── short_report.pdf
│   ├── short_report.tex
│   ├── short_report.tsv
│   └── short_report.txt
└── rnaQuast.sh
```
The program will produce various statistics and it will produce a short summary report as well as a long report, where you can look and assess the quality of your assembled transcriptome. Shown below are matrics which is produced in *short_report.txt* file.

```
SHORT SUMMARY REPORT

METRICS/TRANSCRIPTS                                    centroids

 == BASIC TRANSCRIPTS METRICS ==
Transcripts                                            278727
Transcripts > 500 bp                                   104048
Transcripts > 1000 bp                                  25081
```

rnaQUAST can be used to asses the quality of all the outputs of the Transcriptomic analysis process

- Script to filter sequences shorter than 300 bp from the cds file (the output file from the clustering step vsearch)

Code here: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/Filter300bp.sh*

```
#!/bin/bash
#SBATCH --job-name=filter300
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=15G
#SBATCH --qos=general
#SBATCH -o filter300_%j.out
#SBATCH -e filter300_%j.err
#SBATCH --partition=general

module load seqtk
seqtk seq -L 300 /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/centroids.fasta > centroids300bp.fasta
```

- Match the output of this filtering (cds file) with the pep file (to see if the pep file and the cds file match)

Code here: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/Matchfastapep.sh*

```
#!/bin/bash
#SBATCH --job-name=matchfastapep
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=5G
#SBATCH --qos=general
#SBATCH -o matchfastapep_%j.out
#SBATCH -e matchfastapep_%j.err
#SBATCH --partition=general

module load seqtk
grep ">" centroids300bp.fasta | sed 's/^>//g' > centroids300bpNames.txt
seqtk subseq /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/trinity_combine.fasta.transdecoder.pep centroids300bpNames.txt > filtered300bp_trinity_combine.larch.transdecoder.pep
```
4. **Contaminant filtering**

Now, EnTAP can be run in protein mode (--runP) to perform the contaminant filtering. The reason for using peptides instead of sequences is because we are not working with a model species, we do not know about the alternative splicing and the coding frames of the sequences, so this is why it is better to use the proteins directly to remove the contaminants. Code here: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_example.sh*

```
!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=150G
#SBATCH --qos=general
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general

module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

EnTAP --runP --ini entap_config.ini  -i /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/trinity_combine.fasta.transdecoder.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.209.dmnd --threads 8 --out-dir /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_ContamFilterOutFiles

```
In the *ini* file, it is necessary to put the contaminants we want to filter and the taxon

```
contam=fungi,bacteria,insecta
taxon=Larix_laricina
```

Next steps:

1. Run another contaminant filtering with: 
-	16 cores
-	Add Uniprot database
-	Add Protozoa as contaminant 
And see how it looks like (report in the log file) (if it is different from the previous one)

Code here: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_example_ContamFilteringfixed.sh*

```
#!/bin/bash
#SBATCH --job-name=entap_Cont
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --qos=general
#SBATCH -o entap_Cont%j.out
#SBATCH -e entap_Cont%j.err
#SBATCH --partition=general

module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

EnTAP --runP --ini entap_configFixed.ini  -i /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/filtered300bp_trinity_combine.larch.transdecoder.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.209.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --threads 16 --out-dir /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_ContamFilterOutFilesFixed
```

In the *ini* file, it is necessary to put the contaminants we want to filter and the taxon

```
contam=fungi,bacteria,insecta,amoebozoa
taxon=Larix
```

After running enTAP, I checked the quality of the filtered reference transcriptome in fasta file before running kallisto, using rnaQUAST.

The results are here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/rnaQUASTenTAP

short_report.txt
```
SHORT SUMMARY REPORT 

METRICS/TRANSCRIPTS                                    final_annotations_no_contam_no_duplicatesbyheader  

 == BASIC TRANSCRIPTS METRICS == 
Transcripts                                            209431                                             
Transcripts > 500 bp                                   74555                                              
Transcripts > 1000 bp                                  19243
```

3. Sequences pull out of contaminants, minimum 10-15 million of reads per library

5. **Run kallisto**

The next step is to quantify gene expression. In this tutorial we'll use kallisto, a very fast read pseudo-mapper. Without getting into detail, kallisto is fast, and referred to as a "pseudo-mapper" because it avoids the hard work of a base-level alignment of reads in favor of finding reads' approximate position in the reference transcriptome. This approximation is good enough for RNA-seq (but not for, e.g. variant calling). kallisto will produce estimated counts of reads mapping to transcripts, and estimated transcript abundances, normalized for transcript length. We'll use the estimated counts in later analyses.

This is the working directory for kallisto analysis: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto

5.1. **Creating an index**
The first step is to index our reference transcriptome. An index allows possible read positions in the reference to be looked up very quickly. We'll be working in the 08_Counts directory, and the index script kallisto_index.sh can be executed form that directory by entering sbatch kallisto_index.sh on the command line.

```
kallisto index -i filtered300bp_trinity_combine_final.fasta.index /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_ContamFilterOutFilesFixed/transcriptomes/filtered300bp_trinity_combine_final.fasta
```
5.2. **Counting reads mapping to transcripts**

Kallisto can process either paired-end or single-end reads. The default running mode is paired-end reads and requires a even number of FASTQ files, with pairs given as shown in above example. We provide the index with -i and the number of CPU threads with -t.

Code: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/kallisto.sh

The quantification algorithm will produce three output files:

- abundance.h5 : HDF5 binary file. This contains run information, abundance estimates, bootstrap estimates and transcript lenght information
- abundance.tsv : plaintext file of the abundance estimates. This contains effective length, estimated counts and TPM values
- run_info.json : information on the run

Now if you look at the first few lines in the abundance.tsv file using the head command. We can see that it has five columns: geneID, gene length, effective gene length, estimated counts and tpm.
```

```

Problem with Kallisto, 0 counts in the tsv files. The issue is that there are duplicated reads in the EnTAP output which are confusing kallisto. Plus, the diamond version I used to run EnTAP is outdated. 

## RERUNNING ENTAP

I rerun EnTAP using the following code: 
```
/core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_example_ContamFilteringfixedSophia.sh
```
```
module load EnTAP/0.10.8
module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/2.0.6
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

EnTAP --runP --ini entap_configFixed.ini -i /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/filtered300bp_trinity_combine.larch.transdecoder.pep -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --threads 16 --out-dir /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_ContamFilterOutFilesFixedSophia
```

Output in: */core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_ContamFilterOutFilesFixedSophia/final_results/final_annotations_no_contam.faa*
Number of reads in the output: (grep -c ">" final_annotations_no_contam.faa): 418862
Number of reads in the input: (grep -c ">" filtered300bp_trinity_combine.larch.transdecoder.pep): 276015
Number of contaminants removed: (grep -c ">" final_annotations_contam.faa): 133168

276015*2 - 133168 = 418862 (yes, it is exactly the number of reads on the entap output 418862)

I remove the duplicates on the entap output before runing kallisto to avoid the confusion 

To do so, I use the following codes:

This one remove the duplicates based on the headers name
```
seqkit rmdup < final_annotations_no_contam.faa > final_annotations_no_contam_no_duplicatesbyheader.faa
```
Number of sequences in the output final_annotations_no_contam_no_duplicatesbyheader.faa: 209431 (exactly 418862:2)

I also tried using this code with removes duplicates on the basis of sequence, ignoring differences in headers

```
seqkit rmdup -s < final_annotations_no_contam.faa > final_annotations_no_contam_no_duplicates.faa
```
Number of sequences in the output final_annotations_no_contam_no_duplicates.faa: 209322 (not the half of the input file, so I will usefinal_annotations_no_contam_no_duplicatesbyheader.faa to run kallisto)

But first, this is a protein file of the reference transcriptome, so I have to obtain the sequence file first

To do so, I run this code:
```
module load seqtk
grep ">" final_annotations_no_contam_no_duplicatesbyheader.faa | sed 's/^>//g' > final_annotations_no_contam_no_duplicatesbyheaderNames.txt
seqtk subseq /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/centroids300bp.fasta final_annotations_no_contam_no_duplicatesbyheaderNames.txt > final_annotations_no_contam_no_duplicatesbyheader.fasta
```
I test that the number of reads in the file is correct:
```
grep -c ">" final_annotations_no_contam_no_duplicatesbyheader.fasta 
```
Exactly the same as in the input file: 209431

I also run rnaQUAST

```
SHORT SUMMARY REPORT 

METRICS/TRANSCRIPTS                                    final_annotations_no_contam_no_duplicatesbyheader  

 == BASIC TRANSCRIPTS METRICS == 
Transcripts                                            209431                                             
Transcripts > 500 bp                                   74555                                              
Transcripts > 1000 bp                                  19243
```
Results of rnaQUAST here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/rnaQUASTEnTAP

## BUSCO

I also run BUSCO in both the protein and the fasta file using this code

Code here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/BUSCO

```
#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --qos=general
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err
#SBATCH --partition=general

module load busco/4.0.2
busco -i final_annotations_no_contam_no_duplicatesbyheader.faa -l /isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10 -o busco_larix_protein -m Protein
busco -i final_annotations_no_contam_no_duplicatesbyheader.fasta -l /isg/shared/databases/BUSCO/odb10/lineages/embryophyta_odb10 -o busco_larix_transcriptome -m Transcriptome
```

Results here

Protein file: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/BUSCO/busco_larix_protein
Fasta file: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/BUSCO/busco_larix_transcriptome

BUSCO results for the protein file:

```
        C:88.0%[S:80.9%,D:7.1%],F:4.0%,M:8.0%,n:1614
        1419    Complete BUSCOs (C)
        1305    Complete and single-copy BUSCOs (S)
        114     Complete and duplicated BUSCOs (D)
        64	Fragmented BUSCOs (F)
        131     Missing BUSCOs (M)
        1614    Total BUSCO groups searched
```

BUSCO results for the fasta file:

```
        C:87.6%[S:80.7%,D:6.9%],F:4.0%,M:8.4%,n:1614
        1415    Complete BUSCOs (C)
        1303    Complete and single-copy BUSCOs (S)
        112     Complete and duplicated BUSCOs (D)
        65	Fragmented BUSCOs (F)
        134     Missing BUSCOs (M)
        1614    Total BUSCO groups searched
```
## KALLISTO

Now I can run kallisto:
 Using this code: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/kallistonoduplicates.sh

As a result, we obtained a file per sample containing the abundances per transcript (abundances.tsv)

There are also four techincal replicates per sample (consult)

For the individual K11, four technical replicates: K11L001, K11L002, K11L003, K11L004: This numbers represent the lanes, so rerun kallisto using this fastq files instead: /core/labs/Wegrzyn/Transcriptomics/Larch/7_Bowtie2/input/fastq_files 

Done! The code is here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/kallistonoduplicatesnolanes2.sh

The output of kallisto for each individual is here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto

- Build a count matrix for DESeq2 (https://bioconductor.org/packages/release/bioc/vignettes/tximport/inst/doc/tximport.html): kallisto with the resulting tsv files. I also used Cynthia´s code: https://gitlab.com/PlantGenomicsLab/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine#9-differential-expression-analysis-with-deseq2

```
#Import the libraries needed for the analysis (both count matrix and DE analysis)
library("tximport")
library("readr")
library("DESeq2")

#Set the working directory where you have the tsv files (outputs for kallisto)
outputPrefix <- "larix_deseq2"
setwd("~/Desktop/Postdoc CartograTree/Larch/KallistoBuenoFiles/Larch_tsv")

#Produce a vector with the tsv filenames of the tsv files in the working directory
filenames <- list.files(pattern="*abundance.tsv", full.names=TRUE)
length(filenames)
str(filenames)

#Read the tx2gene matrix that is in the working directory (created using a simple code in Xanadu to get the gene names from the kallisto abundance.tsv files. I have it here:/core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/K12 )
tx2gene <- read.table("tx2gene_larix.tsv", header=TRUE)
dim(tx2gene)
head(tx2gene)

#Create the Count matrix for DESeq2 using the kallisto abundance.tsv files and the tx2gene matrix using "tximport"
txi.kallisto <- tximport(filenames, type = "kallisto", tx2gene = tx2gene, ignoreTxVersion = TRUE)
length(txi.kallisto)
dim(txi.kallisto$counts)
sampleNames <- c("K11","K12","K13","K21","K22","K23","K31","K32","K33","U11","U12","U13","U21","U22","U23","U31","U32","U33")#Add sample names
colnames(txi.kallisto$counts) <- sampleNames
sampleCondition <- c("One","Two","Three","One","Two","Three","One","Two","Three","One","Two","Three","One","Two","Three","One","Two","Three")

#Add the conditions (timepoints)
sampleLocation <- c("K","K","K","K","K","K","K","K","K","U","U","U","U","U","U","U","U","U")#Add the locations K and U
sampleTable <- data.frame(Condition = sampleCondition, Location=sampleLocation, Sample = sampleNames)#Add the locations, conditions and samplenames to the dataframe
sampleTable
ddstxi <- DESeqDataSetFromTximport(txi.kallisto, sampleTable, design = ~ Condition)#Format for DESeq2 and set the design by condition (timempoints)
treatments <-  c("One","Two","Three")

#The variables have to be in "factor" format to be used in the model
colData(ddstxi)$Condition <- factor(colData(ddstxi)$Condition, levels = treatments)
colData(ddstxi)$Location <- factor(colData(ddstxi)$Location)
colData(ddstxi)$Sample <- factor(colData(ddstxi)$Sample)
ddstxi
colData(ddstxi)
```

## Differential expression analysis with DESeq2: Model DESeq to perform DE analysis in a temporal line 

```
#Prefiltering, keep only rows that have at least 10 reads total
keep <- rowSums(counts(ddstxi)) >= 10
length(keep)#197850
dim(ddstxi)#197850 
dds <- ddstxi[keep,]
dim(dds)#193221

colData(dds)
levels(dds$Location)#"K" "U"
levels(dds$Condition)#"One"   "Two"   "Three"
levels(dds$Sample)#"K11" "K12" "K13" "K21" "K22" "K23" "K31" "K32" "K33" "U11" "U12" "U13" "U21" "U22" "U23" "U31" "U32" "U33"

#Multifactor design (timepoints and locations)
ddsMF <- dds #First, I copy the matrix to be able to correct mistakes
ddsMF2 <- dds#Another copy to test other multifactor design, using "Location" as a random effect (we want to test the effect of the timepoint regardless the location)
ddsMF3 <- dds
design(ddsMF) <- formula(~ Location + Condition)
design(ddsMF2) <- formula(~ Condition + (1|Location))
design(ddsMF3) <- formula(~ Condition)#I decided to use only "condition" (timepoints) in the model because we do not want to test the location

ddsMF3
colData(ddsMF3)

#DE analysis for time series experiment
#In order to test for any differences over multiple time points, one can use a design including the time factor, and then test using the likelihood ratio test as described in the following section, where the time factor is removed in the reduced formula (condition in this case)
dds <- DESeq(ddsMF3, test="LRT", reduced=~1)
dim(dds)

#I also try performing a normal DE analysis without using the timepoints
dds2<-DESeq(ddsMF3)

#Cynthia´s code to obtain the results
#Timepoint design
res <- results(dds)
res2vs1<- results (dds, contrast=c("Condition","Two","One"))#Contrast 2 vs 1
res3vs2 <-results (dds, contrast=c("Condition","Three","Two"))#Contrast 3 vs 2
res3vs1 <-results (dds, contrast=c("Condition","Three","One"))#Contrast 3 vs 1

#Normal design
res2 <- results(dds2)
res2_2vs1<- results (dds2, contrast=c("Condition","Two","One"))#Contrast 2 vs 1
res2_3vs2 <-results (dds2, contrast=c("Condition","Three","Two"))#Contrast 3 vs 2
res2_3vs1 <-results (dds2, contrast=c("Condition","Three","One"))#Contrast 3 vs 1

#Summary of the obtained DE genes
#For timepoint design
summary(res)#No DE genes
summary(res2vs1)#No DE genes
summary(res3vs2)#No DE genes
summary(res3vs1)#No DE genes

#For normal design
summary(res2)#LFC > 0 (up):231, 0.12%; LFC < 0 (down):238, 0.12%
summary(res2_2vs1)#adjusted p-value < 0.1 out of 193221 LFC > 0 (up):204, 0.11%; LFC < 0 (down): 541, 0.28%;outliers [1]:70785, 37%;low counts [2]:66213, 34%; mean count < 7
summary(res2_3vs2)#adjusted p-value < 0.1 out of 193221 LFC > 0 (up):485, 0.25%;LFC < 0 (down):170, 0.088%;outliers [1]:70785, 37%;low counts [2]:53330, 28%; mean count < 4
summary(res2_3vs1)#adjusted p-value < 0.1 out of 193221 LFC > 0 (up):231, 0.12%;LFC < 0 (down):238, 0.12%; outliers[1]:70785, 37%;low counts[2]: 60913, 32%; mean count < 6
```

## Results for the DE analysis (the timepoint design did not yield any DE genes, so these are the results for the "normal" pairwise DE analysis comparing 2 vs 1, 3 vs 2 and 3 vs 1)

Adjusted p-value < 0.1 out of 193221 with nonzero total read count

| **Comparison** | **2 vs 1** | **3 vs 2** | **3 vs 1** |
| ------ | ------ | ------ | ------ |
| **Upregulated** | 204, 0.11% | 485, 0.25% | 231, 0.12% |
| **Downregulted** | 541, 0.28% | 170, 0.088% | 238, 0.12% |
| **Outliers** | 70785, 37% | 70785, 37% | 70785, 37% |
| **Low counts** | 66213, 34% | 53330, 28% | 60913, 32% |
| **Mean count** | < 7 | < 4 | < 6 |


However, the lack of DE genes when using the timepoint design was suspicious. As it can be observed in the table at the top of this tutorial, the number of pseudoaligned reads for the samples K32 and K33 is very low (we expect around 10,000,000 for eucariots, and there are 4800624 and 4583835 for K32 and K33, respectively). This is probably due to a high quantity of contaminants. Thus, we decided to repeat the analysis removing these two samples and also removing all samples for the individual K33. We also plot the results in a PCA to visualize how the distribution of the samples changes when removing these samples. By removing individuals K32 and K33 we obtain DE genes when using the timepoint model approach. The code for the PCAs, the PCA plots and the number of DE genes obtained after removing K32 and K33, and all the samples for the individual K3 are shown below:

## Results for the DE analysis using the timepoint design, comparing 2 vs 1, 3 vs 2 and 3 vs 1, after removing K32 and K33

Adjusted p-value < 0.1 out of 155987 with nonzero total read count

| **Comparison** | **2 vs 1** | **3 vs 2** | **3 vs 1** |
| ------ | ------ | ------ | ------ |
| **Upregulated** | 101, 0.065% | 50, 0.032% | 50, 0.032% |
| **Downregulted** | 60, 0.038% | 111, 0.071% | 111, 0.071% |
| **Outliers** | 36319, 23% | 36319, 23% | 36319, 23% |
| **Low counts** | 82759, 53% | 82759, 53% | 82759, 53% |
| **Mean count** | < 10 | < 10 | < 10 |

## Principal Component Analysis 
## All samples

![PCAAllSamplesLabel](PCAAllSamplesLabel.png "PCAAllSamplesLabel")

## After removing K32 and K33

![PCA_NoK32_K33_LabelSmall](PCA_NoK32_K33_LabelSmall.png "PCA_NoK32_K33_LabelSmall")

## After removing all K3 samples

![PCA_noK3_Label](PCA_noK3_Label.png "PCA_noK3_Label")

## Code for the PCA

```
library("genefilter")
library("ggplot2")
library("grDevices")

#Rlog and variance stabilizing count transformation
rld <- rlogTransformation(dds, blind=T)#Warning message recommending using varianceStabilizingTransformation
vsd <- varianceStabilizingTransformation(dds, blind=T)

#PCA plot
rv <- rowVars(assay(rld))
select <- order(rv, decreasing=T)[seq_len(min(500,length(rv)))]
pc <- prcomp(t(assay(vsd)[select,]))
condition<-c("One","Two","Three","One","Two","Three","One","One","Two","Three","One","Two","Three","One","Two","Three")
location<-c("K","K","K","K","K","K","K","U","U","U","U","U","U","U","U","U")
sample<-c("K11","K12","K13","K21","K22","K23","K31","U11", "U12","U13","U21","U22","U23","U31","U32","U33")
scores <- data.frame(pc$x, condition, location,sample)
length(pc)
pc$x
dim(scores)

(pcaplot <- ggplot(scores, aes(x = PC1, y = PC2, label=sample))
  + geom_point(size = 5, aes(shape=location, col=condition))
  + geom_text(hjust=0, vjust=0)
  + ggtitle("Principal Components Larix No K32 and K33")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c("bottom"),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)))
    ggsave(pcaplot,file=paste0(outputPrefix, "-ggplot2.png"))
```

## Volcano plots

![VolcanoPlotLarix1](VolcanoPlotLarix1.png "VolcanoPlotLarix1")

![VolcanoPlot2vs1Big](VolcanoPlot2vs1Big.png "VolcanoPlot2vs1Big")

![VolcanoPlot3vs2Big](VolcanoPlot3vs2Big.png "VolcanoPlot3vs2Big")

![VolcanoPlot3vs1](VolcanoPlot3vs1.png "VolcanoPlot3vs1")

## Heatmaps

![HeatmapLarix](HeatmapLarix.png "HeatmapLarix")

![HeatmapLarixLocationAndIndividual](HeatmapLarixLocationAndIndividual.png "HeatmapLarixLocationAndIndividual")

![HeatmapLarixOrderbyTimepoint](HeatmapLarixOrderbyTimepoint.png "HeatmapLarixOrderbyTimepoint")

## All the R code

```
#Import the libraries needed for the analysis (both count matrix and DE analysis)
library("tximport")
library("readr")
library("DESeq2")

#Set the working directory where you have the tsv files (outputs for kallisto)
outputPrefix <- "larix_deseq2_noK32_K33"
setwd("~/Desktop/Postdoc CartograTree/Larch/KallistoBuenoFiles/Larch_tsv_noK32_K33")

#Produce a vector with the tsv filenames of the tsv files in the working directory
filenames <- list.files(pattern="*abundance.tsv", full.names=TRUE)
length(filenames)
str(filenames)

#Read the tx2gene matrix that is in the working directory (created using a simple code in Xanadu to get the gene names from the kallisto abundance.tsv files. I have it here:/core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/kallisto/K12 )
tx2gene <- read.table("tx2gene_larix.tsv", header=TRUE)
dim(tx2gene)
head(tx2gene)

#Create the Count matrix for DESeq2 using the kallisto abundance.tsv files and the tx2gene matrix using "tximport"
txi.kallisto <- tximport(filenames, type = "kallisto", tx2gene = tx2gene, ignoreTxVersion = TRUE)
length(txi.kallisto)
dim(txi.kallisto$counts)
sampleNames <- c("K11","K12","K13","K21","K22","K23","K31","U11","U12","U13","U21","U22","U23","U31","U32","U33")#Add sample names
colnames(txi.kallisto$counts) <- sampleNames
sampleCondition <- c("One","Two","Three","One","Two","Three","One","One","Two","Three","One","Two","Three","One","Two","Three")#Add the conditions (timepoints)
sampleLocation <- c("K","K","K","K","K","K","K","U","U","U","U","U","U","U","U","U")#Add the locations K and U
sampleTable <- data.frame(Condition = sampleCondition, Location=sampleLocation, Sample = sampleNames)#Add the locations, conditions and samplenames to the dataframe
sampleTable
ddstxi <- DESeqDataSetFromTximport(txi.kallisto, sampleTable, design = ~ Condition)#Format for DESeq2 and set the design by condition (timempoints)
treatments <-  c("One","Two","Three")
#The variables have to be in "factor" format to be used in the model
colData(ddstxi)$Condition <- factor(colData(ddstxi)$Condition, levels = treatments)
colData(ddstxi)$Location <- factor(colData(ddstxi)$Location)
colData(ddstxi)$Sample <- factor(colData(ddstxi)$Sample)
ddstxi
colData(ddstxi)

#Prefiltering, keep only rows that have at least 10 reads total
keep <- rowSums(counts(ddstxi)) >= 10
length(keep)#197850
dim(ddstxi)#197850 
dds <- ddstxi[keep,]
dim(dds)#155987

colData(dds)
levels(dds$Location)#"K" "U"
levels(dds$Condition)#"One"   "Two"   "Three"
levels(dds$Sample)#"K11" "K12" "K13" "K21" "K22" "K23" "K31" "U11" "U12" "U13" "U21" "U22" "U23" "U31" "U32" "U33"

#Multifactor design (timepoints and locations)
ddsMF <- dds #First, I copy the matrix to be able to correct mistakes
ddsMF2 <- dds#Another copy to test other multifactor design, using "Location" as a random effect (we want to test the effect of the timepoint regardless the location)
ddsMF3 <- dds
design(ddsMF) <- formula(~ Location + Condition)
design(ddsMF2) <- formula(~ Condition + (1|Location))
design(ddsMF3) <- formula(~ Condition)#I decided to use only "condition" (timepoints) in the model because we do not want to test the location

ddsMF3
colData(ddsMF3)

#DE analysis for time series experiment
#In order to test for any differences over multiple time points, one can use a design including the time factor, and then test using the likelihood ratio test as described in the following section, where the time factor is removed in the reduced formula (condition in this case)
dds <- DESeq(ddsMF3, test="LRT", reduced=~1)
dim(dds)

#I also try performing a normal DE analysis without using the timepoints
dds2<-DESeq(ddsMF3)

#Cynthia´s code to obtain the results
#Timepoint design
res <- results(dds)
res2vs1<- results (dds, contrast=c("Condition","Two","One"))#Contrast 2 vs 1
res3vs2 <-results (dds, contrast=c("Condition","Three","Two"))#Contrast 3 vs 2
res3vs1 <-results (dds, contrast=c("Condition","Three","One"))#Contrast 3 vs 1

#Normal design
res2 <- results(dds2)
res2_2vs1<- results (dds2, contrast=c("Condition","Two","One"))#Contrast 2 vs 1
res2_3vs2 <-results (dds2, contrast=c("Condition","Three","Two"))#Contrast 3 vs 2
res2_3vs1 <-results (dds2, contrast=c("Condition","Three","One"))#Contrast 3 vs 1

#Summary of the obtained DE genes
#For timepoint design
summary(res)#adjusted p-value < 0.1 out of 155987; LFC > 0 (up) : 50, 0.032%; LFC < 0 (down): 111, 0.071%; outliers [1] : 36319, 23%; low counts [2]: 82759, 53%; mean count < 10
summary(res2vs1)#LFC > 0 (up): 101, 0.065%;LFC < 0 (down): 60, 0.038%;outliers [1]: 36319, 23%;low counts [2]: 82759, 53%;mean count < 10
summary(res3vs2)#LFC > 0 (up):50, 0.032%;LFC < 0 (down): 111, 0.071%;outliers [1]: 36319, 23%;low counts [2]: 82759, 53%;mean count < 10
summary(res3vs1)#LFC > 0 (up): 50, 0.032%;LFC < 0 (down):111, 0.071%;outliers [1]:36319, 23%;low counts [2]:82759, 53%;mean count < 10

#For normal design
summary(res2)#LFC > 0 (up):113, 0.072%; LFC < 0 (down):742, 0.48%;outliers [1]: 37006, 24%;low counts [2]: 70536, 45%;mean count < 6
summary(res2_2vs1)#adjusted p-value < 0.1 out of 155987 LFC > 0 (up):531, 0.34%; LFC < 0 (down): 1304, 0.84%;outliers [1]:37006, 24%;low counts [2]:72551, 47%; mean count < 7
summary(res2_3vs2)#adjusted p-value < 0.1 out of 155987 LFC > 0 (up):441, 0.28%;LFC < 0 (down):461, 0.3%;outliers [1]:37006, 24%;low counts [2]:76482, 49%; mean count < 8
summary(res2_3vs1)#adjusted p-value < 0.1 out of 155987 LFC > 0 (up):113, 0.072%;LFC < 0 (down):742, 0.48%; outliers[1]:37006, 24%;low counts[2]:70536, 45%; mean count < 6

#There are DE genes for the timepoint design, so we use it in the following steps
#In order to plot the results, the adjusted p-values have to be transformed (-log10) so that they become integer numbers (the higher the -log10 pvalue, the more significant)
transf2vs1 <- res2vs1$padj
transf3vs2 <- res3vs2$padj
transf3vs1 <- res3vs1$padj

transfpad2vs1 <- -log10(transf2vs1)
transfpad3vs2 <- -log10(transf3vs2)
transfpad3vs1 <- -log10(transf3vs1)

#Put the transformed adjusted p-values in a dataframe
CFvolData2vs1 <- data.frame("l2fc" = res2vs1$log2FoldChange, "log10padj" = transfpad2vs1)
CFvolData3vs2 <- data.frame("l2fc" = res3vs2$log2FoldChange, "log10padj" = transfpad3vs2)
CFvolData3vs1 <- data.frame("l2fc" = res3vs1$log2FoldChange, "log10padj" = transfpad3vs1)

#Remove the NAs
CFvolData2vs1NoNA <- na.omit(CFvolData2vs1)
CFvolData3vs2NoNA <- na.omit(CFvolData3vs2)
CFvolData3vs1NoNA <- na.omit(CFvolData3vs1)

#Write the dataframe in a csv file
write.table(CFvolData2vs1NoNA, file = "CFvolcanodata2vs1.csv", sep = ",")
write.table(CFvolData3vs2NoNA, file = "CFvolcanodata3vs2.csv", sep = ",")
write.table(CFvolData3vs1NoNA, file = "CFvolcanodata3vs1.csv", sep = ",")

#Order the padj values in the result file
resorder2vs1 <- res2vs1[order(res2vs1$padj),]
resorder3vs2 <- res3vs2[order(res3vs2$padj),]
resorder3vs1 <- res3vs1[order(res3vs1$padj),]

#See how many DE genes per comparision
dim(resorder2vs1)#193221
dim(resorder3vs2)#193221
dim(resorder3vs1)#193221

#Subset the significant DE genes per comparision (padj<0.1)
res2vs1_0.1= subset(res2vs1, padj<0.1)
res3vs2_0.1= subset(res3vs2, padj<0.1)
res3vs1_0.1= subset(res3vs1, padj<0.1)
dim(res2vs1_0.1)
dim(res3vs2_0.1)
dim(res3vs1_0.1)

#Order them
res2vs1_0.1order <- res2vs1_0.1[order(res2vs1_0.1$padj),]
res3vs2_0.1order <- res3vs2_0.1[order(res3vs2_0.1$padj),]
res3vs1_0.1order <- res3vs1_0.1[order(res3vs1_0.1$padj),]

#See how many significant DE genes per comparision (padj<0.1)
dim(res2vs1_0.1order)#161
dim(res3vs2_0.1order)#161
dim(res3vs1_0.1order)#161

#Create a dataframe with the normalized counts of the statistically significant (p<0.1) DE genes per comparison
resdata2vs1 <- merge(as.data.frame(res2vs1_0.1order),
                       as.data.frame(counts(dds,normalized =TRUE)),
                       by = 'row.names', sort = FALSE)
resdata3vs2 <- merge(as.data.frame(res3vs2_0.1order),
                       as.data.frame(counts(dds,normalized =TRUE)),
                       by = 'row.names', sort = FALSE)
resdata3vs1 <- merge(as.data.frame(res3vs1_0.1order),
                       as.data.frame(counts(dds,normalized =TRUE)),
                       by = 'row.names', sort = FALSE)

#Check if the dataframes contain the expected number of statistically significant DE genes per comparison
dim(resdata2vs1)#161
dim(resdata3vs2)#161
dim(resdata3vs1)#161

#Change the name of the first column of the dataframe by "gene"
names(resdata2_2vs1)[1] <- 'gene'
names(resdata2_3vs2)[1] <- 'gene'
names(resdata2_3vs1)[1] <- 'gene'

#Write the dataframe per comparison as a csv and the DE genes results as a txt file
write.csv(resdata2_2vs1, file = paste0(outputPrefix, "-2_2vs1-results-with-normalized.csv"))
write.csv(resdata2_3vs2, file = paste0(outputPrefix, "-2_3vs2-results-with-normalized.csv"))
write.csv(resdata2_3vs1, file = paste0(outputPrefix, "-2_3vs1-results-with-normalized.csv"))

write.table(as.data.frame(counts(dds2),normalized=T), 
            file = paste0(outputPrefix, "_normalized_counts.txt"), sep = '\t')

#Get up and down regulated genes per comparison
pos2vs1 <- subset(res2vs1_0.1order, log2FoldChange > 2)
dim(pos2vs1)
neg2vs1 <- subset(res2vs1_0.1order, log2FoldChange < -2)
dim(neg2vs1)
pos3vs2 <- subset(res3vs2_0.1order, log2FoldChange > 2)
neg3vs2 <- subset(res3vs2_0.1order, log2FoldChange < -2)
pos3vs1 <- subset(res3vs1_0.1order, log2FoldChange > 2)
neg3vs1 <- subset(res3vs1_0.1order, log2FoldChange < -2)

#Get genes that are DE expressed by 4 or by 2
upreg2fold2_2vs1 <- subset(pos2_2vs1, log2FoldChange < 4)
upreg4fold2_2vs1 <- subset(pos2_2vs1, log2FoldChange > 4)
downreg2fold2_2vs1 <- subset(neg2_2vs1, log2FoldChange > -4)
downreg4fold2_2vs1 <- subset(neg2_2vs1, log2FoldChange < -4)

upreg2fold2_3vs2 <- subset(pos2_3vs2, log2FoldChange < 4)
upreg4fold2_3vs2 <- subset(pos2_3vs2, log2FoldChange > 4)
downreg2fold2_3vs2 <- subset(neg2_3vs2, log2FoldChange > -4)
downreg4fold2_3vs2 <- subset(neg2_3vs2, log2FoldChange < -4)

upreg2fold2_3vs1 <- subset(pos2_3vs1, log2FoldChange < 4)
upreg4fold2_3vs1 <- subset(pos2_3vs1, log2FoldChange > 4)
downreg2fold2_3vs1 <- subset(neg2_3vs1, log2FoldChange > -4)
downreg4fold2_3vs1 <- subset(neg2_3vs1, log2FoldChange < -4)

#Create dataframes with the up and down regulated genes by 2 per comparison and write them as .csv
res2up2vs1 <- merge(as.data.frame(pos2vs1),
                      as.data.frame(counts(dds,normalized =TRUE)),
                      by = 'row.names', sort = FALSE)
res2up3vs2 <- merge(as.data.frame(pos3vs2),
                      as.data.frame(counts(dds,normalized =TRUE)),
                      by = 'row.names', sort = FALSE)
res2up3vs1 <- merge(as.data.frame(pos3vs1),
                      as.data.frame(counts(dds,normalized =TRUE)),
                      by = 'row.names', sort = FALSE)

names(res2up2vs1)[1] <- 'gene'
names(res2up3vs2)[1] <- 'gene'
names(res2up3vs1)[1] <- 'gene'

write.csv(res2up2vs1, file = paste0(outputPrefix, "-2vs1-upreg2fold.csv"))
write.csv(res2up3vs2, file = paste0(outputPrefix, "-3vs2-upreg2fold.csv"))
write.csv(res2up3vs1, file = paste0(outputPrefix, "-3vs1-upreg2fold.csv"))

res2down2vs1 <- merge(as.data.frame(neg2vs1),
                        as.data.frame(counts(dds,normalized =TRUE)),
                        by = 'row.names', sort = FALSE)
res2down3vs2 <- merge(as.data.frame(neg3vs2),
                        as.data.frame(counts(dds,normalized =TRUE)),
                        by = 'row.names', sort = FALSE)
res2down3vs1 <- merge(as.data.frame(neg3vs1),
                        as.data.frame(counts(dds,normalized =TRUE)),
                        by = 'row.names', sort = FALSE)

names(res2down2vs1)[1] <- 'gene'
names(res2down3vs2)[1] <- 'gene'
names(res2down3vs1)[1] <- 'gene'

write.csv(res2down2vs1, file = paste0(outputPrefix, "-2vs1-downreg2fold.csv"))
write.csv(res2down3vs2, file = paste0(outputPrefix, "-3vs2-downreg2fold.csv"))
write.csv(res2down3vs1, file = paste0(outputPrefix, "-3vs1-downreg2fold.csv"))

#Plot the DE analysis (Volcano plot)
#All samples
plotMA(dds, ylim=c(-8,8),main = "RNAseq Larix")
dev.copy(png, paste0(outputPrefix, "-MAplot_initial_analysis.png"))
dev.off()

#2 vs 1
alpha <- 0.1
cols2vs1 <- densCols(res2vs1$log2FoldChange, -log10(res2vs1$pvalue))
plot(res2vs1$log2FoldChange, -log10(res2vs1$padj), col=cols2vs1, panel.first=grid(),
     main="Volcano plot 2 vs 1", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)
abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")

gn.selected <- abs(res2vs1$log2FoldChange) > 2.5 & res2vs1$padj < alpha 
text(res2vs1$log2FoldChange[gn.selected],
     -log10(res2vs1$padj)[gn.selected],
     lab=rownames(res2vs1)[gn.selected ], cex=0.4)

#3 vs 2
alpha <- 0.1
cols3vs2 <- densCols(res3vs2$log2FoldChange, -log10(res3vs2$pvalue))
plot(res3vs2$log2FoldChange, -log10(res3vs2$padj), col=cols3vs2, panel.first=grid(),
     main="Volcano plot 3 vs 2", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)
abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")

gn.selected <- abs(res3vs2$log2FoldChange) > 2.5 & res3vs2$padj < alpha 
text(res3vs2$log2FoldChange[gn.selected],
     -log10(res3vs2$padj)[gn.selected],
     lab=rownames(res3vs2)[gn.selected ], cex=0.4)

#3 vs 1
alpha <- 0.1
cols3vs1 <- densCols(res3vs1$log2FoldChange, -log10(res3vs1$pvalue))
plot(res3vs1$log2FoldChange, -log10(res3vs1$padj), col=cols3vs1, panel.first=grid(),
     main="Volcano plot 3 vs 1", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)
abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")

gn.selected <- abs(res3vs1$log2FoldChange) > 2.5 & res3vs1$padj < alpha 
text(res3vs1$log2FoldChange[gn.selected],
     -log10(res3vs1$padj)[gn.selected],
     lab=rownames(res3vs1)[gn.selected ], cex=0.4)

#Rlog and variance stabilizing count transformation
rld <- rlogTransformation(dds, blind=T)#Warning message recommending using varianceStabilizingTransformation
vsd <- varianceStabilizingTransformation(dds, blind=T)

rld2 <- rlogTransformation(dds2, blind=T)#Warning message recommending using varianceStabilizingTransformation
vsd2 <- varianceStabilizingTransformation(dds2, blind=T)
write.csv(as.data.frame(assay(rld)),file = paste0(outputPrefix, "-rlog-transformed-counts.txt"))
write.csv(as.data.frame(assay(vsd)),file = paste0(outputPrefix, "-vst-transformed-counts.txt"))

par(mai = ifelse(1:4 <= 2, par('mai'),0))
px <- counts(dds)[,1] / sizeFactors(dds)[1]
dim(dds)
length(px)
ord <- order(px)
ord <- ord[px[ord] < 150]
ord <- ord[seq(1,length(ord),length=50)]
last <- ord[length(ord)]
vstcol <- c('blue','black')
matplot(px[ord], cbind(assay(vsd)[,1], log2(px))[ord, ],type='l', lty = 1, col=vstcol, xlab = 'n', ylab = 'f(n)')
legend('bottomright',legend=c(expression('variance stabilizing transformation'), expression(log[2](n/s[1]))), fill=vstcol)
dev.copy(png,paste0(outputPrefix, "-variance_stabilizing.png"))
dev.off()

#Replace outliers
ddsClean <- replaceOutliersWithTrimmedMean(dds)
ddsClean <- DESeq(ddsClean)
tab <- table(initial = results(dds)$padj < 0.1,
             cleaned = results(ddsClean)$padj < 0.1)
addmargins(tab)
write.csv(as.data.frame(tab),file = paste0(outputPrefix, "-replaceoutliers.csv"))
resClean <- results(ddsClean)
resClean0.1 = subset(res, padj<0.1)
resClean0.1order <- resClean0.1[order(resClean0.1$padj),]
write.csv(as.data.frame(resClean0.1order),file = paste0(outputPrefix, "-replaceoutliers-results.csv"))

#PCA plot no K32 K33 Timepoint model (el bueno)
library("genefilter")
library("ggplot2")
library("grDevices")
length(rv)
rv
treatments
condition
rv <- rowVars(assay(rld))
select <- order(rv, decreasing=T)[seq_len(min(500,length(rv)))]
pc <- prcomp(t(assay(vsd)[select,]))
condition<-c("One","Two","Three","One","Two","Three","One","One","Two","Three","One","Two","Three","One","Two","Three")
location<-c("K","K","K","K","K","K","K","U","U","U","U","U","U","U","U","U")
sample<-c("K11","K12","K13","K21","K22","K23","K31","U11", "U12","U13","U21","U22","U23","U31","U32","U33")
scores <- data.frame(pc$x, condition, location,sample)
length(pc)
pc$x
dim(scores)

(pcaplot <- ggplot(scores, aes(x = PC1, y = PC2, label=sample))
  + geom_point(size = 5, aes(shape=location, col=condition))
  + geom_text(hjust=0, vjust=0)
  + ggtitle("Principal Components Larix No K32 and K33")
  + scale_colour_brewer(name = " ", palette = "Set1")
  + theme(
    plot.title = element_text(face = 'bold'),
    legend.position = c("bottom"),
    legend.key = element_rect(fill = 'NA'),
    legend.text = element_text(size = 10, face = "bold"),
    axis.text.y = element_text(colour = "Black"),
    axis.text.x = element_text(colour = "Black"),
    axis.title.x = element_text(face = "bold"),
    axis.title.y = element_text(face = 'bold'),
    panel.grid.major.x = element_blank(),
    panel.grid.major.y = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.minor.y = element_blank(),
    panel.background = element_rect(color = 'black',fill = NA)))
ggsave(pcaplot,file=paste0(outputPrefix, "-ggplot2.png"))

#Heatmap 
head(assay(rld))
plot(log2(1+counts(dds,normalized=T)[,1:2]),col='black',pch=20,cex=0.3, main='Log2 transformed')
plot(assay(rld)[,1:2],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")
plot(assay(rld)[,3:4],col='#00000020',pch=20,cex=0.3, main = "rlog transformed")
library("RColorBrewer")
library("gplots")
select <- order(rowMeans(counts(ddsClean,normalized=T)),decreasing=T)[1:30]
select2 <- order(rowMeans(counts(dds,normalized=T)),decreasing=T)[1:30]
my_palette <- colorRampPalette(c("blue",'white','red'))(n=30)
heatmap(assay(vsd)[select,], col=my_palette,
        scale="row", key=T, keysize=1, symkey=T,
        density.info="none", trace="none",
        cexCol=0.6, labRow=F,
        main="Larix")
heatmap(assay(vsd)[select2,], col=my_palette,
        scale="row", key=T, keysize=1, symkey=T,
        density.info="none", trace="none",
        cexCol=0.6, labRow=F,
        main="Larix2")
heatmap(assay(vsd)[select2,], col=my_palette,
        scale="row", key=T, keysize=1, symkey=T,
        density.info="none", trace="none",
        cexCol=0.6, labRow=F,
        main="Larix Order by Location and Individual", Colv = NA, Rowv = NA)
vsd
Assay<-assay(vsd)[select2,]
AssayorderbyTimepoint<-Assay[,c(1,4,7,8,11,14,2,5,9,12,15,3,6,10,13,16)]
AssayorderbyTimepoint
heatmap(AssayorderbyTimepoint, col=my_palette,
        scale="row", key=T, keysize=1, symkey=T,
        density.info="none", trace="none",
        cexCol=0.6, labRow=F,
        main="Larix Order by Timepoint", Colv = NA, Rowv = NA)
dev.copy(png, paste0(outputPrefix, "-distanceHEATMAP.png"))
dev.off()
```
## pHeatmaps, Heatmap sample to sample and PCA using other code

### pHeatmap using 20 genes
![pHeatmap20genes](pHeatmap20genes.png "pHeatmap20genes")

### pHeatmap using 100 genes
![pHeatmap100genes](pHeatmap100genes.png "pHeatmap100genes")

### pHeatmap using all genes
![pHeatmapAllGenes](pHeatmapAllGenes.png "pHeatmapAllGenes")

### pHeatmap sample to sample
![HeatmapSample-to-sample](HeatmapSample-to-sample.png "HeatmapSample-to-sample")

### PCA labeled and with percentages
![PCAPercentagevsdBueno](PCAPercentagevsdBueno.png "PCAPercentagevsdBueno")

## R code 

```
#pHeatmap. Code from here: http://bioconductor.org/packages/devel/bioc/vignettes/DESeq2/inst/doc/DESeq2.html#multi-factor-designs
#20 genes
#install.packages("pheatmap")
library("pheatmap")
ntd <- normTransform(dds)
select <- order(rowMeans(counts(dds,normalized=TRUE)),
                decreasing=TRUE)[1:20]
dim(dds)
head(dds)
df <- as.data.frame(colData(dds)[,c("Condition","Location")])
pheatmap(assay(ntd)[select,], cluster_rows=FALSE, show_rownames=FALSE,
         cluster_cols=FALSE, annotation_col=df)

#All genes
library("pheatmap")
ntd <- normTransform(dds)
select <- order(rowMeans(counts(dds,normalized=TRUE)),
                decreasing=TRUE)
dim(dds)
head(dds)
df <- as.data.frame(colData(dds)[,c("Condition","Location")])
dim(df)
head(df)
pheatmap(assay(ntd)[select,], cluster_rows=FALSE, show_rownames=FALSE,
         cluster_cols=FALSE, annotation_col=df)

#100 genes
library("pheatmap")
ntd <- normTransform(dds)
select <- order(rowMeans(counts(dds,normalized=TRUE)),
                decreasing=TRUE)[1:100]
dim(dds)
head(dds)
df <- as.data.frame(colData(dds)[,c("Condition","Location")])
pheatmap(assay(ntd)[select,], cluster_rows=FALSE, show_rownames=FALSE,
         cluster_cols=FALSE, annotation_col=df)

#Heatmap of the sample-to-sample distances
sampleDists <- dist(t(assay(vsd)))

library("RColorBrewer")
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(vsd$Condition, vsd$Location, sep="-")
colnames(sampleDistMatrix) <- NULL
colors <- colorRampPalette( rev(brewer.pal(9, "Blues")) )(255)
pheatmap(sampleDistMatrix,
         clustering_distance_rows=sampleDists,
         clustering_distance_cols=sampleDists,
         col=colors)

#Another PCA
pcaData <- plotPCA(vsd, intgroup=c("Condition", "Location","Sample"), returnData=TRUE)
percentVar <- round(100 * attr(pcaData, "percentVar"))
ggplot(pcaData, aes(PC1, PC2, color=Condition, shape=Location, label=Sample)) +
  geom_point(size=3) +
  geom_text(hjust=0, vjust=0)+
  xlab(paste0("PC1: ",percentVar[1],"% variance")) +
  ylab(paste0("PC2: ",percentVar[2],"% variance")) + 
  coord_fixed()
```

### New PCA and heatmaps using this tutorial: https://github.com/CBC-UCONN/RNA-seq-with-reference-genome-and-annotation
### PCA using variance-stabilized transformed counts blind False (instead of TRUE as previously)
![PCA2_Bigger_PercentageLabel](PCA2_Bigger_PercentageLabel.png "PCA2_Bigger_PercentageLabel")

### Heatmap using regularized log transformation of counts blind False (instead of TRUE as previously) and the 30 most important genes selected using the absolute value of shrunken log2 fold change (excluding cook's cutoff outliers)
![HeatmapRLDnormalized30genes](HeatmapRLDnormalized30genes.png "HeatmapRLDnormalized30genes")

### Same heatmap but using vst transformation blind FALSE instead (warning message)
![HeatmapVSDnormalization30genes](HeatmapVSDnormalization30genes.png "HeatmapVSDnormalization30genes")

### Same heatmap using regularized log transformation of counts blind False but re-scaling regularized log-scaled counts by baseMean (estimated mean across all samples)
![HeatmapRe-scaledBaseMean](HeatmapRe-scaledBaseMean.png "HeatmapRe-scaledBaseMean")

### Same heatmap using regularized log transformation of counts blind False but scaling by the average expression level in the control samples in our reference condition, One.

The code is lacking in the reference tutorial

### Code used for these PCA and heatmaps

```
#Plotting PCA and Heatmaps using another code and strategy: https://github.com/CBC-UCONN/RNA-seq-with-reference-genome-and-annotation
# get shrunken log fold changes
install.packages("ashr")
res_shrink2vs1 <- lfcShrink(dds,type="ashr",coef="Condition_Two_vs_One")
res_shrink3vs1 <- lfcShrink(dds,type="ashr",coef="Condition_Three_vs_One")

res2vs1<- results (dds, contrast=c("Condition","Two","One"))#Contrast 2 vs 1
resultsNames(dds)

# plot the shrunken log2 fold changes against the raw changes:
library("tidyverse")
data.frame(l2fc=res$log2FoldChange, l2fc_shrink=res_shrink2vs1$log2FoldChange, padj=res$padj) %>%
  filter(l2fc > -5 & l2fc < 5 & l2fc_shrink > -5 & l2fc_shrink < 5) %>%
  ggplot(aes(x=l2fc, y=l2fc_shrink,color=padj > 0.1)) +
  geom_point(size=.25) + 
  geom_abline(intercept=0,slope=1, color="gray")

# get the top 20 genes by shrunken log2 fold change
arrange(data.frame(res_shrink2vs1), log2FoldChange) %>% head(., n=20)

plotMA(res_shrink2vs1, ylim=c(-4,4))

# negative log-scaled adjusted p-values
log_padj2vs1 <- -log(res_shrink2vs1$padj,10)
log_padj2vs1[log_padj2vs1 > 100] <- 100

log_padj3vs1 <- -log(res_shrink3vs1$padj,10)
log_padj3vs1[log_padj3vs1 > 100] <- 100

# plot
plot(x=res_shrink2vs1$log2FoldChange,
     y=log_padj2vs1,
     pch=20,
     cex=.5,
     col=(log_padj2vs1 > 10)+1, # color padj < 0.1 red
     ylab="negative log-scaled adjusted p-value",
     xlab="shrunken log2 fold changes")

#PCA
# normalized, variance-stabilized transformed counts for visualization (blind False)
vsdF <- vst(dds, blind=FALSE)

# alternatively, using ggplot

dat <- plotPCA(vsdF,returnData=TRUE,intgroup=c("Condition","Location", "Sample"))

p <- ggplot(dat,aes(x=PC1,y=PC2,col=Condition, shape=Location, label=Sample))
p <- p + geom_point() + 
  xlab(paste("PC1: ", round(attr(dat,"percentVar")[1],2)*100, "% variation explained", sep="")) + 
  ylab(paste("PC2: ", round(attr(dat,"percentVar")[2],2)*100, "% variation explained", sep="")) +
  geom_text(hjust=0, vjust=0)

#Heatmap
# regularized log transformation of counts
rldF <- rlog(dds, blind=FALSE)

# order gene names by absolute value of shrunken log2 fold change (excluding cook's cutoff outliers)
lfcorder <- data.frame(res_shrink2vs1) %>%
  filter(!is.na(padj)) %>% 
  arrange(-abs(log2FoldChange)) %>% 
  rownames() 

# create a metadata data frame to add to the heatmaps
df <- data.frame(colData(dds)[,c("Condition","Location")])
rownames(df) <- colnames(dds)
colnames(df) <- c("Condition","Location")

# use regularized log-scaled counts
library(pheatmap)
pheatmap(
  assay(rldF)[lfcorder[1:30],], 
  cluster_rows=TRUE, 
  show_rownames=TRUE,
  cluster_cols=TRUE,
  annotation_col=df
)
  
#Warning message regarding rld normalization. It recomends using vst instead. 
#I am repeating the heatmap using this normalization method

library(pheatmap)
pheatmap(
  assay(vsdF)[lfcorder[1:30],], 
  cluster_rows=TRUE, 
  show_rownames=TRUE,
  cluster_cols=TRUE,
  annotation_col=df
)

#Even more messy. Let´s try this
# re-scale regularized log-scaled counts by baseMean (estimated mean across all samples)
pheatmap(
  assay(rldF)[lfcorder[1:30],] - log(res[lfcorder[1:30],"baseMean"],2), 
  cluster_rows=TRUE, 
  show_rownames=TRUE,
  cluster_cols=TRUE,
  annotation_col=df
)

#we can try scaling by the average expression level in the control samples in our reference condition, One
pheatmap(
  assay(rldF)[lfcorder[1:30],] - log(res[lfcorder[1:30],"One"],2), 
  cluster_rows=TRUE, 
  show_rownames=TRUE,
  cluster_cols=TRUE,
  annotation_col=df
)
```

7. GOseq: Gene ontology enrichement, for non-model species

Text files containing the gene lengths and the GO terms per gene are required to run GOseq

To obtain the gene lengths, the following code was run here: /core/labs/Wegrzyn/Transcriptomics/Larch/ContaminantFilteringAmeeIreneDef/entap_ContamFilterOutFilesFixedSophia/final_results

````
seqkit fx2tab --length --name --header-line -o genelength.txt final_annotations_no_contam_no_duplicatesbyheader.fasta
````

